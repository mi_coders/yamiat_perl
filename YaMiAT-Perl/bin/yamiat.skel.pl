#!/usr/bin/perl
BEGIN {
	use FindBin;
	unshift @INC, ( $FindBin::Bin, "$FindBin::Bin/../lib" );

	#use Getopt::Std;
	#getopts('v',  \%opts);
	#unless (@ARGV){
	#   print "USAGE: $FindBin::Script [-v] domain\n";
	#   exit;
	#}
}

use YAMIAT;
use REST::Client;
use MIME::Base64;
use XML::LibXML;

my $a = YAMIAT->new();

my $dbh = $a->getPhoneDBHandle();

my $query = "SELECT CPE, ID, APIUSER, APIPASS, FQDN FROM customer where EUS=1";
my $sth   = $dbh->prepare($query);
$sth->execute;
print "Select Customer:\n";
$i = 1;
print "0) exit\n";
while ( $res = $sth->fetchrow_hashref ) {
	print $i . ") $res->{CPE}\n";
	push @cust, $res;
	$i++;
}
$in = <STDIN>;
chomp($in);
if ( $in == 0 ) {
	exit(0);
}

print "enter userid:\n";
$userid = <STDIN>;
chomp $userid;
if ( $userid =~ m/\r/ ) {
	$userid =~ s/\r//;
}

my $APIURL  = "https://$cust[$in-1]->{'FQDN'}/api/v1/dm/users/$userid";
my $APIAUTH = 'Basic ' 
	. +encode_base64(
		$cust[ $in - 1 ]->{'APIUSER'} 
		. ":" . $cust[ $in - 1 ]->{'APIPASS'}
	);
$a->setActiveCustomer($cust[$in-1]->{'FQDN'}, $APIAUTH, $cust[$in-1]->{'ADMINMAIL'});

my $client = REST::Client->new();
$client->addHeader( 'Accept',        'application/xml' );
$client->addHeader( 'Authorization', $APIAUTH );
$client->GET($APIURL);
if ( $client->responseCode() == 404 ) {
	print "no user found, exiting\n";
	exit(0);
}
my $ret    = $client->responseContent();
my $parser = XML::LibXML->new();
my $doc    = $parser->parse_string($ret);
my $xquery = "//device";
my $count  = $doc->findvalue("count($xquery)");
#if ( $count > 1 ) {

	# more than 1 device, we need to select the "desired" one
	$dev = 1;
	print "Device(s) found, select from list:\n\n";
	print "\t Device \t MSISDN \t Status \t Compliance\t\n";
	foreach my $device ( $doc->findnodes($xquery) ) {
		my ($uid) = $device->findnodes('./uuid');
		push @devices, $uid->to_literal;
		my $phone  = $device->findnodes('./model/text()');
		my $msisdn = $device->findnodes('./phoneNumber/text()');
		my $status = $device->findnodes('./status/text()');
		my ($compliance) = $device->findnodes('./quarantinedStatus/text()');
		if ($compliance->to_literal == 0){
			$compliance="COMPLIANT";
		}else{
			$compliance="NOT COMPLIANT";
		}
		print $dev . ") \t $phone \t $msisdn \t $status \t $compliance\n";
		$dev++;
	}
	$deviceuuid = <STDIN>;
	chomp $deviceuuid;
	if ( $deviceuuid =~ m/\r/ ) {
		$deviceuuid =~ s/\r//;
	}
	
	if ($deviceuuid <= $count){
			$deviceuuid = $devices[ $deviceuuid - 1 ];
	}else{
		print "illegal value entered, exiting\n";
		exit;
	}


#}
#else {
#	my ($uuid) = $doc->findnodes('./uuid/text()');
#	$deviceuuid = $uuid->to_literal;
#	print "single device found with uuid: $deviceuuid\n";
#}

#get device details
my $APIURL_DETAIL = "https://$cust[$in-1]->{'FQDN'}/api/v1/dm/devices/$deviceuuid";
my $client_DETAIL = REST::Client->new();
$client_DETAIL->addHeader( 'Accept',        'application/xml' );
$client_DETAIL->addHeader( 'Authorization', $APIAUTH );
$client_DETAIL->GET($APIURL_DETAIL);
if ( $client_DETAIL->responseCode() == 404 ) {
	print "no device found, exiting\n";
	exit(0);
}
my $ret_DETAIL = $client_DETAIL->responseContent();
my $rparser    = XML::LibXML->new();
my $rdoc       = $rparser->parse_string($ret_DETAIL);
my $printout="Device Information:\n\n";
$printout = $printout . "MSISDN / PDA:   \t" . $rdoc->findnodes('//phoneNumber/text()') . "\n";
$printout = $printout . "Username:       \t" . $rdoc->findnodes('//principal/text()') . "\n";
$printout = $printout . "User Full Name: \t" . $rdoc->findnodes('//userDisplayName/text()') . "\n";
$printout = $printout .  "E-Mail:         \t" . $rdoc->findnodes('//emailAddress/text()') . "\n";
$printout = $printout .  "Last Connection:\t" . $rdoc->findnodes('//lastConnectedAt/text()') . "\n";
$printout = $printout .  "Manufacturer:   \t" . $rdoc->findnodes('//manufacturer/text()') . "\n";
$printout = $printout .  "Model:          \t" . $rdoc->findnodes('//model/text()') . "\n";
my ($quarantine) = $rdoc->findnodes('//quarantinedStatus/text()');
$printout = $printout .  $a->getCompilanceInformation( $quarantine->to_literal );
print $printout;
print "\n\nchoose action:\n";
print "0) Exit\n";
print "1) Wipe\n";
print "2) Re-Register\n";

my $action = <STDIN>;
if ( $action == 0 ) {
	exit(0);
}
if ($action == 1){
	print "wiping device... ";
	$wipecode = $a->wipeDevice($deviceuuid, $printout);
	$a->log_message("Device wipe action for device $deviceuuid on host $a->{HOST}, $wipecode ");
	print $wipecode."\n";
	exit(0);
}
if ($action == 2){
	print "reregistering device";
	$retcode=$a->reregisterDevice($deviceuuid, $rdoc, $printout);
	$a->log_message("Device reregister action for device $deviceuuid on host $a->{HOST}, $retcode ");
	print $retcode."\n";
	exit(0);
}

