#!/usr/bin/perl
BEGIN {
	use FindBin;
	unshift @INC, ( $FindBin::Bin, "$FindBin::Bin/../lib" );

	#use Getopt::Std;
	#getopts('c',  \%opts);
	unless (@ARGV) {
		print "USAGE: $FindBin::Script file\n";
		print "Example: $FindBin::Script input.csv";
		print
"The input is to be assumed to have only imei numbers, seperated by newline";
		exit;
	}
}

use YAMIAT;
use REST::Client;
use MIME::Base64;
use XML::LibXML;

#works just for this customer, specific development
my $cnn = "fubi";

my $host = undef;
my $apiuser = undef;
my $apipass = undef;
my $APIAUTH = undef;

my $a = YAMIAT->new();

my $dbh = $a->getPhoneDBHandle();

#empty the tmp-tables
my $rv = $dbh->do("delete from tmpDeviceMI;");
my $rv = $dbh->do("delete from tmpDeviceCSV;");

#getting the api connection info
my $query =
  "SELECT ID, APIUSER, APIPASS, HOSTIP FROM customer where CPE like ?";
my $sth = $dbh->prepare($query);
$sth->execute($cnn);

#preparing query for inserting all info to the temp table
my $miquery = "insert into tmpDeviceMI (uuid, IMEI, UID) values (?, ?, ?)";
my $misth   = $dbh->prepare($miquery);

my $csvquery ="insert into tmpDeviceCSV (IMEI) values (?)";
my $csvsth = $dbh->prepare($csvquery);


while ( $res = $sth->fetchrow_hashref ) {
	$apiuser = $res->{'APIUSER'};
	$apipass = $res->{'APIPASS'};
	$host = $res->{'HOSTIP'};
	#getting all active devices from mobileiron
	my $APIURL = "https://$res->{'HOSTIP'}/api/v1/dm/devices/";
	$APIAUTH =
	  'Basic ' . encode_base64( $res->{'APIUSER'} . ":" . $res->{'APIPASS'} );
	my $client = REST::Client->new();
	$client->addHeader( 'Accept',        'application/xml' );
	$client->addHeader( 'Authorization', $APIAUTH );
	$client->GET($APIURL);
	my $ret    = $client->responseContent();
	my $parser = XML::LibXML->new();
	my $doc    = $parser->parse_string($ret);
	my $xquery = "//device";

	foreach my $device ( $doc->findnodes($xquery) ) {

		my $imei = $device->findnodes(
			".//key[contains(fn:upper-case(.),\"IMEI\")]/following-sibling::value/text()");
		my $user = $device->findnodes(".//principal/text()");
		my $uuid = $device->findnodes(".//uuid/text()");

		$imei =~ s/\s//g;

		$misth->execute( $uuid, $imei, $user );

	}
}

#read in the csv and bring it into the database
open F, $ARGV[0] or die "cannot open file: $ARGV[0]";
while (<F>) {
	$_=~s/-//g;
	$csvsth->execute($_);
}

#compare the two sets and print the information to stdout
#TODO: retire all devices, except for users in SA environment
$query4="select MI.uuid, MI.UID from tmpDeviceMI MI right outer join tmpDeviceCSV CSV on MI.IMEI=CSV.IMEI where CSV.IMEI=0";
$sth2 = $dbh->prepare($query4);
$sth2->execute();
while ($res2 = $sth2->fetchrow_hashref){
	my $APIURL = "https://$host/api/v1/dm/devices/$res2->{'uuid'}";
	my $client4 = REST::Client->new();
	$client4->addHeader( 'Accept',        'application/xml' );
	$client4->addHeader( 'Authorization', $APIAUTH );
	$client4->GET($APIURL);
	my $ret4   = $client4->responseContent();
	print $ret4;
}


