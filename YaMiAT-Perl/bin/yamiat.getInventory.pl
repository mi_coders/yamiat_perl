#!/usr/bin/perl
BEGIN {
	use FindBin;
	unshift @INC, ( $FindBin::Bin, "$FindBin::Bin/../lib" );

	#use Getopt::Std;
	#getopts('c',  \%opts);
	unless (@ARGV) {
		print "USAGE: $FindBin::Script file\n";
		print "Example: $FindBin::Script input.csv";
		print
"The input is to be assumed to have only imei numbers, seperated by newline";
		exit;
	}
}

use YAMIAT;
use REST::Client;
use MIME::Base64;
use XML::LibXML;

#works just for this customer, specific development
my $cnn = "fubi";

my $host = undef;
my $apiuser = undef;
my $apipass = undef;
my $APIAUTH = undef;

my $a = YAMIAT->new();

my $dbh = $a->getPhoneDBHandle();

#empty the tmp-tables
my $rv = $dbh->do("delete from tmpDeviceMI;");
my $rv = $dbh->do("delete from tmpDeviceCSV;");

#getting the api connection info
my $query =
  "SELECT ID, APIUSER, APIPASS, HOSTIP FROM customer where CPE like ?";
my $sth = $dbh->prepare($query);
$sth->execute($cnn);

#preparing query for inserting all info to the temp table
my $miquery = "insert into tmpDeviceMI (uuid, IMEI, UID) values (?, ?, ?)";
my $misth   = $dbh->prepare($miquery);

my $csvquery ="insert into tmpDeviceCSV (IMEI) values (?)";
my $csvsth = $dbh->prepare($csvquery);


while ( $res = $sth->fetchrow_hashref ) {
	$apiuser = $res->{'APIUSER'};
	$apipass = $res->{'APIPASS'};
	$host = $res->{'HOSTIP'};
	#getting all active devices from mobileiron
	my $APIURL = "https://$res->{'HOSTIP'}/api/v1/dm/devices/";
	$APIAUTH =
	  'Basic ' . encode_base64( $res->{'APIUSER'} . ":" . $res->{'APIPASS'} );
	my $client = REST::Client->new();
	$client->addHeader( 'Accept',        'application/xml' );
	$client->addHeader( 'Authorization', $APIAUTH );
	$client->GET($APIURL);
	my $ret    = $client->responseContent();
	my $parser = XML::LibXML->new();
	my $doc    = $parser->parse_string($ret);
	print $doc;
}

