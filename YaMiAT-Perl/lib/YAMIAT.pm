# Author Frank Harenberg, frank@harenberg.ch, 2006
# Licenced under the GPL v2
# The licence is located in the licence directory

package YAMIAT;

$|++;

use strict;
use DBI;
use REST::Client;
use MIME::Base64;
use XML::LibXML;
use Net::SMTP;
use XML::Simple;

use vars qw($Config @EXPORT $dbhpool @ISA $VERSION);
require Exporter;

@ISA = qw(Exporter Autoloader);

$VERSION = '0.01';

sub new {
	my $self;
	my $class;
	my $file;
	unless ($file) {
		my $confDir = $INC{'YAMIAT.pm'};
		$confDir =~ s/lib\/YAMIAT.pm/conf/;
		$file = "$confDir/yamiat.prod.conf.xml";
		unless ( -e $file ) {
			die "No config file found, searching at $file\n";
		}
	}
	my $simple = XML::Simple->new();
	$Config             = $simple->XMLin($file);
	$class              = $Config;
	$class->{HOST}      = undef;
	$class->{AUTH}      = undef;
	$class->{ADMINMAIL} = undef;
	bless $class;
	return $class, $Config;
}

sub log_message {
	my $self = shift;
	my $msg  = shift;
	my $logger;
	if ( -e "/bin/logger" ) {
		$logger = "/bin/logger";
	}
	if ( -e "/usr/bin/logger" ) {
		$logger = "/usr/bin/logger";
	}
	my $cmd = "$logger -i -p local3.info -t $FindBin::Script --  $msg";
	system($cmd);
}

sub getDBHandle {
	my $self = shift;
	my ( $host, $db, $user, $pass ) = @_;

	my $dsn = "DBI:mysql:database=$db;host=$host";
	return DBI->connect_cached( $dsn, $user, $pass )
	  || die("cannot create db connection, check config values");
}

sub getPhoneDBHandle {
	my $self = shift;
	my $dbh  = $self->getDBHandle(
		$self->{database}->{host}, $self->{database}->{dbname},
		$self->{database}->{user}, $self->{database}->{pass}
	);
	return $dbh;
}

sub setActiveCustomer {
	my $self = shift;
	my ( $host, $auth, $adminmail ) = @_;
	$self->{HOST} = $host;
	$self->{AUTH} = $auth;
	if ($adminmail) {
		$self->{ADMINMAIL} = $adminmail;
	}

}

sub sendMail {
	my $self = shift;
	my $msg  = shift;

	#We only send mail if the recipients mailadress has been defined
	unless ( $self->{ADMINMAIL} ) {
		my $smtp = NET::SMTP->new( $self->{smtp}->{smarthost} );

		#Authenticate to smarthost if credentials are within config file
		my $user = $self->{smtp}->{user};
		my $pass = $self->{smtp}->{pass};
		unless ( $self->{smtp}->{user} ) {
			$smtp->auth( $user, $pass );
		}
		$smtp->mail( $self->{smtp}->{sender} );
		$smtp->to( $self->{ADMINMAIL} );
		$smtp->data();
		$smtp->datasend("$msg");
		$smtp->dataend();

	}

}

sub wipeDevice {
	my $self   = shift;
	my $uuid   = shift;
	my $msg    = shift;
	my $APIURL = "https://$self->{HOST}/api/v1/dm/devices/wipe/$uuid";
	my $client = REST::Client->new();
	$client->addHeader( 'Accept',        'application/xml' );
	$client->addHeader( 'Authorization', $self->{AUTH} );
	$client->PUT($APIURL);
	return $client->responseCode();

	$self->sendMail(
		"A wipe request has been issued for following the following device:\n"
		  . $msg );
}

sub reregisterDevice {
	my $self = shift;
	my $uuid = shift;
	my $user = shift;
	my $msg  = shift;

	my $client = REST::Client->new();
	$client->addHeader( 'Accept',        'application/xml' );
	$client->addHeader( 'Authorization', $self->{AUTH} );
	$client->PUT("https://$self->{HOST}/api/v1/dm/devices/retire/$uuid");
	if ( $client->responseCode() == 200 ) {

		#retire successful, register
		my $phonenumber = $user->findnodes('//phoneNumber/text()');
		my $userId      = $user->findnodes('//principal/text()');
		my $platform;
		my $deviceType;
		my $ldapsource       = "True";
		my $userFistName     = $user->findnodes('//userFirstName/text()');
		my $userLastName     = $user->findnodes('//userLastName/text()');
		my $userEmailAddress = $user->findnodes('//emailAddress/text()');
		my $notifyUser       = "True";

		if ( $user->findnodes('//phoneNumber/text()') =~ m/PDA/ ) {
			$deviceType = "PDA";
		}
		else {
			$deviceType = "phone";
		}

		if ( $user->findnodes('//platform/text()') =~ m/iOS/ ) {
			$platform = "I";
		}
		if ( $user->findnodes('//platform/text()') =~ m/Android/i ) {
			$platform = "A";
		}

		my $uri =
		    'https://'
		  . $self->{HOST}
		  . '/api/v1/dm/register?'
		  . 'phoneNumber='
		  . $phonenumber . '&'
		  . 'userId'
		  . $userId . '&'
		  . 'platform'
		  . $platform . '&'
		  . 'deviceType'
		  . $deviceType . '&'
		  . 'importUserFromLdap'
		  . $ldapsource . '&'
		  . 'userFirstName'
		  . $userFistName . '&'
		  . 'userLastName'
		  . $userLastName . '&'
		  . 'userEmailAddress'
		  . $userEmailAddress . '&'
		  . 'notifyUser'
		  . $notifyUser;

		$client->PUT($uri);
		if ( $client->responseCode() != 200 ) {
			return "410";
		}
	}
	
	$self->sendMail(
		"A new registration pin has been issued for following the following device:\n"
		  . $msg );
	
	return $client->responseCode();

}

sub getCompilanceInformation {
	my $self = shift;
	my $var  = shift;
	my $retval;
	if ( $var == 0 ) {
		$retval = "COMPLIANT\n";
	}
	else {
		$retval = "NOT COMPLIANT, code $var, reason:\n";
		if ( $var >= 4194304 ) {
			$var    = $var - 4194304;
			$retval = $retval . "reason unknown\n";
		}
		if ( $var >= 16384 ) {
			$var    = $var - 16384;
			$retval = $retval . "Disallowed app found\n";
		}
		if ( $var >= 8012 ) {
			$var    = $var - 8012;
			$retval = $retval . "required app missing\n";
		}
		if ( $var >= 4096 ) {
			$var    = $var - 4096;
			$retval = $retval . "forbidden app found\n";
		}
		if ( $var >= 2048 ) {
			$var    = $var - 2048;
			$retval = $retval . "MDM deactivated by user\n";
		}
		if ( $var >= 1024 ) {
			$var    = $var - 1024;
			$retval = $retval . "Exchange reported\n";
		}
		if ( $var >= 512 ) {
			$var    = $var - 512;
			$retval = $retval . "Device blocked by administrator\n";
		}
		if ( $var >= 256 ) {
			$var    = $var - 256;
			$retval = $retval . "Device not registered\n";
		}
		if ( $var >= 128 ) {
			$var    = $var - 128;
			$retval = $retval . "Device exceeds mailbox limit\n";
		}
		if ( $var >= 64 ) {
			$var    = $var - 64;
			$retval = $retval . "App control policy is out of compliance.\n";
		}
		if ( $var >= 32 ) {
			$var    = $var - 32;
			$retval = $retval . "Device out of contact\n";
		}
		if ( $var >= 16 ) {
			$var    = $var - 16;
			$retval = $retval . "Policy outdated\n";
		}
		if ( $var >= 8 ) {
			$var    = $var - 8;
			$retval = $retval . "Device not encrypted\n";
		}
		if ( $var >= 4 ) {
			$var    = $var - 4;
			$retval = $retval . "Hardware version not allowed (iOS)\n";
		}
		if ( $var >= 2 ) {
			$var    = $var - 2;
			$retval = $retval . "OS too old\n";
		}
		if ( $var >= 1 ) {
			$var    = $var - 1;
			$retval = $retval . "Device compromised\n";
		}

	}

	return $retval;
}

sub DESTROY {
	1;
}

1;
__END__
 
